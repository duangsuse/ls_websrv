#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;
use rocket::State;

#[get("/")]
fn hello_world() -> &'static str {
    "Hello, world!"
}
#[get("/rule/<pkg>")]
fn get_rule(pkg: Srting) -> String {

}
#[get("/rev/<pkg>")]
fn get_rev(pkg: String) -> String {

}
#[get("/update/<tok>")]
fn do_update(secuity: String) -> String {

}

struct Loc {
    pub l: String,
    pub m: DefaultOpt,
}
struct Rule {
    pub pkg: String,
    pub rev: u32,
    pub ver: String,
    pub desc: String,
    pub content: Vec<Loc>
}
enum DefaultOpt {
    Non,
    Del,
    Rep,
}
fn query_repo() -> Vec<Rule> {

}

fn main() {
    rocket::ignite().mount("/", routes![hello_world]).launch();
}
